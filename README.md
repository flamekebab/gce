# GCE

## Description
See the GCE page on the Unnamed Gorkamorka Site (tUGS) for details: https://gorkamorka.co.uk/gce/

## Authors and acknowledgment
These files were created by the GCE Kommittee - the members have varied and for who worked on each book see the final page of each document.

## License
Each document in the documents directory contains its own licence details on the final page.
The files in the others directory are under a https://creativecommons.org/licenses/by-nc-sa/4.0/ licence.
templates-a4.psd is attributable to Morgan Fox and Jenny Mathiasson, the others files are attributable to Morgan Fox alone.
